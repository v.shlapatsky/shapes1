﻿using ShapesATasks;

Console.WriteLine("Tests for your tasks");

Console.WriteLine("Task1");
Console.Write("a = 1, ");
Tasks.Task1(1);

Console.Write("a = 3, ");
Tasks.Task1(3);

Console.Write("a = 5, ");
Tasks.Task1(5);


Console.WriteLine("Task2");
Console.Write("a = 2, b = 4, ");
Tasks.Task2(2, 4);

Console.Write("a = 3, b = 5, ");
Tasks.Task2(3, 5);

Console.Write("a = 1, b = 4, ");
Tasks.Task2(1, 4);


Console.WriteLine("Task3");
Console.Write("a = 1, ");
Tasks.Task3(1);

Console.Write("a = 2, ");
Tasks.Task3(2);

Console.Write("a = 5, ");
Tasks.Task3(5);

Console.ReadKey();

