﻿namespace ShapesATasks
{
    public static class Tasks
    {
        public static void Task1(int a)
        {
            int perimeter = 0;
            perimeter = 4 * a;


            Console.WriteLine($"P = {perimeter}");
        }

        public static void Task2(int a, int b)
        {
            int perimeter = 0, area = 0;
            perimeter = 2 * (a + b);
            area = a * b;


            Console.WriteLine($"P = {perimeter}, S = {area}");
        }

        public static void Task3(int a)
        {
            int volume = 0, surfaceArea = 0;
            volume = Convert.ToInt32(Math.Pow(a, 3));
            surfaceArea = 6 * Convert.ToInt32(Math.Pow(a, 2));


            Console.WriteLine($"V = {volume}, S = {surfaceArea}");
        }

    }
}
