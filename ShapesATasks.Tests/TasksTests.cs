using NUnit.Framework;
using System;
using System.IO;

namespace ShapesATasks.Tests
{
    [TestFixture]
    public class TasksTests
    {
        private StringWriter writer;

        [SetUp]
        public void SetUp()
        {
            writer = new StringWriter();
            Console.SetOut(this.writer);
        }

        [TearDown]
        public void Cleanup()
        {
            this.writer.Close();
        }

        [TestCase(1, "P = 4")]
        [TestCase(3, "P = 12")]
        [TestCase(5, "P = 20")]
        public void Task1_ReturnsCorrectValue(int a, string expected)
        {
            Tasks.Task1(a);
            var actual = this.writer.GetStringBuilder().ToString().Trim();
            Assert.AreEqual(expected, actual, "Task1 returns incorrect value.");
        }


        [TestCase(2, 4, "P = 12, S = 8")]
        [TestCase(3, 5, "P = 16, S = 15")]
        [TestCase(1, 4, "P = 10, S = 4")]
        public void Task2_ReturnsCorrectValue(int a, int b, string expected)
        {
            Tasks.Task2(a, b);
            var actual = this.writer.GetStringBuilder().ToString().Trim();
            Assert.AreEqual(expected, actual, "Task2 returns incorrect value.");
        }        


        [TestCase(1, "V = 1, S = 6")]
        [TestCase(2, "V = 8, S = 24")]
        [TestCase(5, "V = 125, S = 150")]
        public void Task3_ReturnsCorrectValue(int a, string expected)
        {
            Tasks.Task3(a);
            var actual = this.writer.GetStringBuilder().ToString().Trim();
            Assert.AreEqual(expected, actual, "Task3 returns incorrect value.");
        }
    }
}
