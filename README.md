# Linear calculations (Shapes 1)

## Task 1
The square's side is **_a_**. Find its perimeter using the formula: **P = 4 × a**
```
Example: а = 7   perimeter = 28
```

## Task 2
The rectangle's sides are **_a_** and **_b_**. Find its perimeter using the formula: **P = 2×(a + b)**. 

Find its area following the formula: **S = a × b**
```
Example: а = 5, b = 3   perimeter = 16
         а = 5, b = 3   area = 15
```

## Task 3
The cube`s side is **_a_**. Find its volume using the formula: **V = a³**. 

Find its surface area following the formula: **S = 6 × a²**
```
Example: а = 5   volume = 125
         а = 5   surface area = 150
```
